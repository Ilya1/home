# 1FitChat, Базовая часть функционала 

[Регистрация пользователя](#markdown-header-)  
[Авторизация пользователя](#markdown-header-_1)  
[Восстановление пароля](#markdown-header-_2)  

[Обновление профиля пользователя](#markdown-header-_3)  
[Просмотр профиля выбранного пользователя](#markdown-header-_4)  

[Получение реквизитов для загрузки файлов на S3](#markdown-header-s3)  

[Получение группы текущего пользователя](#markdown-header-_5)  
[Получение консультанта текущего пользователя](#markdown-header-_6)  

[Получение всех анкет пользователя](#markdown-header-_7)  
[Отправка ответов на выбранную анкету](#markdown-header-_8)  
[Получение результатов анкет для выбранного пользователя](#markdown-header-_9)

[Сохранение токена устройства ios](#markdown-header-ios)  
[Сохранение токена устройства android](#markdown-header-android)  

## Регистрация пользователя
Тип запроса: **POST**  
Относительный URL запроса: **/rest/register**  
Параметры функции:

* **phone** - Номер телефона пользователя (regex:~^[0-9]{11,12}$~|required|unique:users,phone)

Ответ, **"Ошибка регистрации"**
```json
{
     "result": "fail",
     "errors": [
        "Этот номер телефона уже зарегистрирован."
     ]
}
```

Ответ, **"Регистракция успешно пройдена"**
```json
{
    "result": "ok"
}
```

## Авторизация пользователя
Тип запроса: **POST**  
Относительный URL запроса: **/rest/auth**  
Параметры функции:

* **phone** - Номер телефона пользователя (string)
* **password** - Пароль пользователя (string)

Ответ, **"Ошибка авторизации"**
```json
{
    "result": "fail",
    "errors": [
        "Ошибка авторизации, проверьте ваш пароль и номер телефона"
    ]
}
```

Ответ, **"Успешная авторизация"**
```json
{
    "result": "ok",
    "userInfo": {
        "id": "430",
        "name": "Ilya Rogojin",
        "sex": "male",
        "birthday": "1982-01-01",
        "phone": "79261534545",
        "role": "user",
        "info": "",
        "token": "e4f7fcbc8433350c4782cc3fcfae29d9Nq0182",
        "avatar_large_url": "",
        "avatar_small_url": ""
    }
}
```

## Восстановление пароля
Тип запроса: **POST**  
Относительный URL запроса: **/rest/restore-password**  
Параметры функции:

* **phone** - Номер телефона пользователя (regex:~^[0-9]{11,12}$~|required, string)

Ответ, **"Ошибка восстановления пароля 1"**
```json
{
    "result": "fail",
    "errors": [
        "Нужно ввести корректный номер телефона."
    ]
}
```

Ответ, **"Ошибка восстановления пароля 2"**
```json
{
    "result": "fail",
    "errors": [
        "Указанный номер телефона не зарегистрирован"
    ]
}
```

Ответ, **"Новый пароль успешно отправлен в SMS"**
```json
{
    "result": "ok"
}
```

## Обновление профиля пользователя
Тип запроса: **POST**  
Относительный URL запроса: **/rest/base/update-profile**  
Параметры функции:

* **token** - Токен, выданный функцией **/rest/auth**  
* **name** - Имя пользователя (string|min:5|required)
* **sex** - Пол пользователя (in:male,female|required)
* **birthday** - Дата рождения пользователя (date|required, 1990-12-20)
* **avatar_large_url** - Урл "большого" аватара пользователя на amazon (url|sometimes|required_with:avatar_small_url, 180x200px)
* **avatar_small_url** - Урл "маленького" аватара пользователя на amazon (url|sometimes|required_with:avatar_large_url, 48x48px)

Ответ, **"Ошибка обновления профиля"**
```json
{
    "result": "fail",
    "errors": [
        "Поле Имя пользователя должно быть заполнено.",
        "Поле Пол должно быть заполнено.",
        "Поле Дата рождения должно быть заполнено."
    ]
}
```

Ответ, **"Обновление профиля успешно"**
```json
{
    "result": "ok",
    "userInfo": {
        "id": "430",
        "name": "Ilya Rogojin",
        "sex": "male",
        "birthday": "1980-12-20",
        "avatar_large_url": "http://r0.ru",
        "avatar_small_url": "http://ya.ru"
    }
}
```

## Просмотр профиля выбранного пользователя
Тип запроса: **GET**  
Относительный URL запроса: **/rest/base/user-profile?token={token}&id={id}**  
Параметры функции:

* **token** - Токен, выданный функцией **/rest/auth**  
* **id** - Идентификатор выбранного пользователя (integer)

Ответ, **"Пользователь не найден"**
```json
{
    "result": "fail",
    "errors": [
        "Пользователь не найден"
    ]
}
```

Ответ, **"Профиль пользователя"**
```json
{
    "result": "ok",
    "userInfo": {
        "id": "1",
        "name": "Илья Рогожин",
        "sex": "male",
        "birthday": "1982-02-02",
        "phone": "79261534545",
        "role": "admin",
        "info": "Разработчик, Арканит",
        "avatar_large_url": "https://s3-us-west-2.amazonaws.com/fitconsultant-test3/avatar/180x200/1.jpg",
        "avatar_small_url": "https://s3-us-west-2.amazonaws.com/fitconsultant-test3/avatar/48x48/1.jpg",
        "registered": "28.08.2015 10:54:04",
        "age": "33 года",
        "blocked": "0",
        "paid_access": "1",
        "paid_untill": "2016-02-06"
    }
}
```

## Получение реквизитов для загрузки файлов на S3
Тип запроса: **GET**  
Относительный URL запроса: **/rest/base/s3-credentials?token={token}**  
Параметры функции:

* **token** - Токен, выданный функцией **/rest/auth**

Пример использования этих реквизитов приведен ниже по ссылке  
http://www.designedbyaturtle.co.uk/2015/direct-upload-to-s3-using-aws-signature-v4-php/

Ответ, **"Реквизиты для загрузки файлов на S3"**
```json
{
    "result": "ok",
    "credentials": {
        "acl": "public-read",
        "success_status": "201",
        "policy": "eyJleHB....dfQ==",
        "algorithm": "AWS4-HMAC-SHA256",
        "credentials": "AKIAJCAHS4OYNSAJ7QWQ/20150729/us-east-1/s3/aws4_request",
        "date": "20150729T133855Z",
        "expires": "86400",
        "signature": "4eda6a3127e435e6629dfb97880994e34f11e91d05f2b0320659e9c5b6a11bb2",
        "AWSAccessKeyId": "AKIAJCAHS4OYNSAJ7QWQ",
        "url": "//s3.amazonaws.com/fitconsultant-test2"
    }
}
```

## Получение группы текущего пользователя
Тип запроса: **GET**  
Относительный URL запроса: **/rest/base/group-of-user?token={token}**  
Параметры функции:

* **token** - Токен, выданный функцией **/rest/auth**

Ответ, **"Ошибка, группа не найдена"**
```json
{
    "result": "fail",
    "errors": [
        "Группа не найдена"
    ]
}
```

Ответ, **"Информация о группе пользователя"**
```json
{
    "result": "ok",
    "groupInfo": {
        "id": "55a509aff814e63e078b456a",
        "name": "Комната для новичков",
        "info": "Это комната для всех наших новичков",
        "avatar_large_url": "",
        "avatar_small_url": ""
    }
}
```

## Получение консультанта текущего пользователя
Тип запроса: **GET**  
Относительный URL запроса: **/rest/base/consultant-of-user?token={token}**  
Параметры функции:

* **token** - Токен, выданный функцией **/rest/auth**

Ответ, **"Ошибка, чат не найден"**
```json
{
    "result": "fail",
    "errors": [
        "Чат не найден"
    ]
}
```

Ответ, **"Ошибка, консультант не найден"**
```json
{
    "result": "fail",
    "errors": [
        "Консультант не найден"
    ]
}
```

Ответ, **"Информация о консультанте пользователя"**
```json
{
    "result": "ok",
    "consultantInfo": {
        "id": "1",
        "name": "Test admin",
        "info": "admin dummy info",
        "avatar_large_url": "",
        "avatar_small_url": ""
    }
}
```

## Получение всех анкет пользователя
Тип запроса: **GET**  
Относительный URL запроса: **/rest/base/questionnaires?token={token}**  
Параметры функции:

* **token** - Токен, выданный функцией **/rest/auth**

Типы вопросов:

* **text** - текстовый, в ответе будет ожидаться строка в поле **text**
* **select_one** - выбор одного варианта, в ответе будет ожидаться 
один идентификатор ответа из соответствующего вопросу набора ответов
* **select_many** - выбор нескольких вариантов, в ответе будет ожидаться 
набор идентификаторов ответов из соответствующего вопросу набора ответов

Ответ, **"Все доступные анкеты"**
```json
[
    {
        "id": "1",
        "name": "Первый опрос",
        "questions": [
            {
                "id": "1",
                "type": "text",
                "name": "Вопрос номер один (текст)"
            },
            {
                "id": "2",
                "type": "select_one",
                "name": "Вопрос номер два (выбор одного)",
                "replies": [
                    {
                        "id": "1",
                        "text": "Вариант ответа 1"
                    },
                    {
                        "id": "2",
                        "text": "Вариант ответа 2"
                    },
                    {
                        "id": "3",
                        "text": "Вариант ответа 3"
                    }
                ]
            },
            {
                "id": "3",
                "type": "select_many",
                "name": "Вопрос номер три (выбор многих)",
                "replies": [
                    {
                        "id": "4",
                        "text": "Вариант ответа 1 (мн)"
                    },
                    {
                        "id": "5",
                        "text": "Вариант ответа 2 (мн)"
                    },
                    {
                        "id": "6",
                        "text": "Вариант ответа 3 (мн)"
                    },
                    {
                        "id": "7",
                        "text": "Вариант ответа 4 (мн)"
                    },
                    {
                        "id": "8",
                        "text": "Вариант ответа 5 (мн)"
                    }
                ]
            }
        ]
    },
    {
        "id": "3",
        "name": "Третий опрос",
        "questions": [
            {
                "id": "4",
                "type": "text",
                "name": "Вопрос номер один (текстовый)"
            },
            {
                "id": "5",
                "type": "text",
                "name": "Вопрос номер два (тоже текстовый)"
            },
            {
                "id": "6",
                "type": "select_one",
                "name": "Вопрос номер три (выбор одного варианта)",
                "replies": [
                    {
                        "id": "9",
                        "text": "Вариант ответа на третий вопрос (один)"
                    },
                    {
                        "id": "10",
                        "text": "Вариант ответа на третий вопрос (два)"
                    },
                    {
                        "id": "11",
                        "text": "Вариант ответа на третий вопрос (три)"
                    }
                ]
            }
        ]
    }
]
```


## Отправка ответов на выбранную анкету
Тип запроса: **POST**  
Относительный URL запроса: **/rest/base/send-questionnaire**  
Параметры функции:

* **token** - Токен, выданный функцией **/rest/auth**
* **quest_id** - Идентификатор выбранной для ответов анкеты
* **questions** - Массив ответов на вопросы анкеты (вариант)
* **questions_json** - Массив ответов на вопросы ананкеты в формате json (вариант)

Можно отправлять ответы в массиве POST переменных **questions** или
строкой в формате json в одной POST переменной **questions_json**  
Ключ в json-ответе соответствует идентификатору вопроса, значение
ключа указывает на тип ответа и соответствующее значение, варианты
разных типов ответов приведены в примере ниже.  
Пример отправки ответов в формате json для поля **questions_json**
```json
{
    "1": {
        "type": "text",
        "text": "я люблю печенье"
    },
    "2": {
        "type": "select_one",
        "reply_id": "1"
    },
    "3": {
        "type": "select_many",
        "reply_ids": [
            "5",
            "7"
        ]
    }
}
```

Ниже приведен пример HTML формы для отправки ответов на вопросы первой анкеты (см. данные анкеты в 
описаниие метода **Получение всех анкет**).

Пример HTML формы анкеты можно посмотреть по относительному адресу:  
**/rest/base/questionnaire-form-example/{questionnaire_id}**

В случае если тип ответа текстовый, ответ ожидается в виде:  
**questions[question_id] = 'text value'**  
В случае если тип ответа "выбор одного", ответ ожидается в виде:  
**questions[question_id] = reply_id**  
В случае если тип ответа "выбор многих", ответ ожидается в виде:  
**questions[question_id][reply_id] = 1**  
**questions[question_id][reply_id] = 1**  
**questions[question_id][reply_id] = 1**  
```html
<form method="post" action="http://fit-consultant.php.dev/rest/base/send-questionnaire">
  <input type="hidden" name="quest_id" value="1">
  <h1>Первый опрос</h1>
  <h4>Вопрос номер один (текст)</h4>
  <input type="text" name="questions[1]" value="">
  <h4>Вопрос номер два (выбор одного)</h4>
  <ul>
    <li>
      <input type="radio" name="questions[2]" value="1"> Вариант ответа 1
    </li>
    <li>
      <input type="radio" name="questions[2]" value="2"> Вариант ответа 2
    </li>
    <li>
      <input type="radio" name="questions[2]" value="3"> Вариант ответа 3
    </li>
  </ul>
  <h4>Вопрос номер три (выбор многих)</h4>
  <ul>
    <li>
      <input type="checkbox" name="questions[3][4]" value="1"> Вариант ответа 1 (мн)
    </li>
    <li>
      <input type="checkbox" name="questions[3][5]" value="1"> Вариант ответа 2 (мн)
    </li>
    <li>
      <input type="checkbox" name="questions[3][6]" value="1"> Вариант ответа 3 (мн)
    </li>
    <li>
      <input type="checkbox" name="questions[3][7]" value="1"> Вариант ответа 4 (мн)
    </li>
    <li>
      <input type="checkbox" name="questions[3][8]" value="1"> Вариант ответа 5 (мн)
    </li>
  </ul>
  <br><br><br>
  <input type="submit" value="Отправить ответы">
</form>
```

## Получение результатов анкет для выбранного пользователя
Тип запроса: **GET**  
Относительный URL запроса: **/rest/base/replied-user-quests?token={token}&user_id={user_id}**  
Параметры функции:

* **token** - Токен, выданный функцией **/rest/auth**
* **user_id** - Идентификатор пользователя, для которого нужно посмотреть результаты

В примере ниже, ответ содержит массив ответов на вопросы всех отвеченных анкет для
выбранного пользователя. Первый ключ "1" - идентификатор анкеты, ключи следующего уровня
"1", "2", "3" - идентификаторы вопросов анкеты "1". Внутри каждого вопроса, в зависимости
от типа вопроса указан вариант ответа: в поле "text" для текстовых вопросов, в поле
"reply_id" для вопросов с одним вариантом ответа и массив значений в поле "reply_ids"
для вопросов с возможностью множественного выбора ответов.

Ответ, **"Все результаты анкет пользователя"**
```json
{
    "result": "ok",
    "user_replies": {
        "1": {
            "1": {
                "type": "text",
                "text": "я люблю печенье"
            },
            "2": {
                "type": "select_one",
                "reply_id": "1"
            },
            "3": {
                "type": "select_many",
                "reply_ids": [
                    "5",
                    "7"
                ]
            }
        }
    }
}
```

## Сохранение токена устройства ios
Тип запроса: **POST**  
Относительный URL запроса: **/rest/base/save-device-token-ios**  
Параметры функции:

* **token** - Токен, выданный функцией **/rest/auth**
* **device_token** - Токен устройства ios

Ответ, **"Токен устройства ios сохранен"**
```json
{
    "result": "ok"
}
```

## Сохранение токена устройства android
Тип запроса: **POST**  
Относительный URL запроса: **/rest/base/save-device-token-android**  
Параметры функции:

* **token** - Токен, выданный функцией **/rest/auth**
* **device_token** - Токен устройства ios

Ответ, **"Токен устройства ios сохранен"**
```json
{
    "result": "ok"
}
```
