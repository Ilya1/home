# 1FitChat, Функционал для платежей через cloudpayments.ru

[Статус возможности онлайн оплаты подписки](#markdown-header-)  
[Доступные сервисы](#markdown-header-_1)  
[Запрос на оплату](#markdown-header-_2)  

## Статус возможности онлайн оплаты подписки
Тип запроса: **GET**  
Относительный URL запроса: **/rest/base/payment-status**  
Параметры функции:

* **token** - Токен, выданный функцией **/rest/auth**

Ответ, **"Возможность онлайн оплаты включена"**
```json
{
    "result": "ok",
    "online_payment": "on"
}
```

Ответ, **"Возможность онлайн оплаты выключена"**
```json
{
    "result": "ok",
    "online_payment": "off"
}
```

## Доступные сервисы
Тип запроса: **GET**  
Относительный URL запроса: **/rest/payment/services**  
Параметры функции:

* **token** - Токен, выданный функцией **/rest/auth**

Ответ, **"Доступные сервисы"**
```json
{
    "result": "ok",
    "services": [
        {
            "id": "1week",
            "desc": "Еженедельно",
            "duration_text": "1 неделя",
            "price": 2500
        },
        {
            "id": "1month",
            "desc": "Ежемесячно",
            "price": 10000,
            "duration_text": "1 месяц"
        }
    ]
}
```

## Запрос на оплату
Тип запроса: **POST**  
Относительный URL запроса: **/rest/payment/payment**  
Параметры функции:

* **token** - Токен, выданный функцией **/rest/auth**
* **service_id** - идентификатор услуги из функции **/rest/payment/services**
* **cryptogram** - криптограмма пользователя на основании его реквизитов

**REM** вероятно в ответе будет поле с указанием оплаченного периода подписки

Ответ, **"Ошибка сервиса"**
```json
{
    "result": "fail",
    "serviceResponse": {
        "Success": false,
        "Message": "Invalid Name"
    }
}
```

Ответ, **"Требуется подтверждение 3ds"**
```json
{
    "result": "fail",
    "term_url": "http://fit-consultant.php.dev/rest/payment/term-url",
    "serviceResponse": {
        "Model": {
            "TransactionId": 1498259,
            "PaReq": "eyJNZXJjaGFudE5hbW...S1SVSJ9",
            "AcsUrl": "https://demo.cloudpayments.ru/acs",
            "IFrameIsAllowed": true
        },
        "InnerResult": null,
        "Success": false,
        "Message": null
    }
}
```

Ответ, **"Платеж успешно завершен"**
```json
{
    "result": "ok",
    "term_url": "http://fit-consultant.php.dev/rest/payment/term-url",
    "serviceResponse": {
        "Model": {
            "PublicId": "pk_77b1a9d4e54ed9b6d6e3fe8380bae",
            "TransactionId": 1491349,
            "Amount": 18700,
            "Currency": "RUB",
            "CurrencyCode": 0,
            "PaymentAmount": 18700,
            "PaymentCurrency": "RUB",
            "PaymentCurrencyCode": 0,
            "InvoiceId": "1",
            "AccountId": "1",
            "Email": null,
            "Description": null,
            "JsonData": null,
            "LocalOrder": false,
            "CreatedDate": "/Date(1444264410254)/",
            "CreatedDateIso": "2015-10-08T00:33:30",
            "AuthDate": "/Date(1444264410497)/",
            "AuthDateIso": "2015-10-08T00:33:30",
            "ConfirmDate": "/Date(1444264410497)/",
            "ConfirmDateIso": "2015-10-08T00:33:30",
            "AuthCode": "A1B2C3",
            "TestMode": true,
            "Rrn": "a6a0f40c-349e-4403-bd86-37b5aa4c20c8",
            "IpAddress": "127.0.0.1",
            "IpCountry": null,
            "IpCity": null,
            "IpRegion": null,
            "IpDistrict": null,
            "IpLatitude": null,
            "IpLongitude": null,
            "CardFirstSix": "424242",
            "CardLastFour": "4242",
            "CardExpDate": "10/17",
            "CardType": "Visa",
            "CardProduct": null,
            "CardCategory": null,
            "IssuerBankCountry": null,
            "Issuer": null,
            "CardTypeCode": 0,
            "OriginalTransactionId": null,
            "Status": "Completed",
            "StatusCode": 3,
            "Reason": "Approved",
            "ReasonCode": 0,
            "CardHolderMessage": "Оплата успешно проведена",
            "Type": 0,
            "Refunded": false,
            "Name": "ILYA ROGOZHIN",
            "Token": "477BBA133C182267FE5F086924ABDC5DB71F77BFC27F01F2843F2CDC69D89F05",
            "Data": null,
            "SubscriptionId": null
        },
        "InnerResult": null,
        "Success": true,
        "Message": null
    }
}
```

**REM**  
Схема взаимодействия описана здесь:  
https://cloudpayments.ru/Docs/Api#payWithCrypto  
Тестовые данные для карт здесь:  
https://cloudpayments.ru/Docs/Test  
Коды ошибок здесь:  
https://cloudpayments.ru/Docs/ErrorCodes

В общем виде последовательность такая:  

1. получаем список услуг из ф-ии **/rest/payment/services**
1. выбираем услугу и вызываем с её идентификатором ф-ю оплаты **/rest/payment/payment**
1. в случае успеха - заканчиваем
1. в случае ошибки - проверяем что это запрос на 3ds подтверждение или стандартная ошибка
1. для случая стандартной ошибки, смотрим поля ReasonCode и CardHolderMessage
1. для случая с 3ds - из ответа получаем поля TransactionId, PaReq, AcsUrl и term_url.  
Далее берем параметры (полученное название поля -> новое имя поля)  
PaReq -> **PaReq**, TransactionId -> **MD**, term_url -> **TermUrl**  
(выделенные имена полей должны быть в оправляемом запросе)  
все эти параметры со значениями отправляем POST-ом по ссылке, полученной в параметре AcsUrl.  
Привем при отправке этой формы, перед получением ответа произойдет редирект на наш сервер.
1. В ответе получим те же варианты ответов что и в методе **/rest/payment/payment**  

**REM**  
Для разработчиков веб-части: генератор криптограммы и форму для тестовой 
отправки данных для 3ds подтверждения можно найти в папке  
**project-etc/payment-test-helpers**