# 1FitChat, Настройки cron

```
# m h  dom mon dow   command

### tasks for dev-zone
* * * * * php /home/fitcons/www-data/dev-zone/artisan schedule:run 1>> /dev/null 2>&1

### tasks for stage-zone
* * * * * php /home/fitcons/www-data/stage-zone/artisan schedule:run 1>> /dev/null 2>&1

### DBs backup
0 4,18 * * * /home/fitcons/backup/backup_1fitchat.sh
```