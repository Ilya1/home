# 1FitChat, Настройки для Debian 7

## Ставим серверный софт

**Правим sources**  
```
vim /etc/apt/sources/list
```
добавляем:  
```
# backports
deb http://http.debian.net/debian wheezy-backports main
# php 5.5
deb http://packages.dotdeb.org wheezy-php55 all
deb-src http://packages.dotdeb.org wheezy-php55 all
```

**Добавляем ключи**
```
wget http://www.dotdeb.org/dotdeb.gpg
apt-key add dotdeb.gpg

apt-get install debian-keyring debian-archive-keyring
apt-get update
```

**VIM**
```
apt-get purge vim
apt-get -t wheezy-backports install vim
```

**GIT**
```
apt-get -t wheezy-backports install git
```

**MC**
```
apt-get purge mc
apt-get -t wheezy-backports install mc zip
```

**PHP 5.5**
```
apt-get install php5-cli php5-mcrypt php5-fpm  php5-curl php5-gd php5-mysql php5-imagick php5-readline
apt-get install php5-mongo
```

**NGINX**
```
apt-get -t wheezy-backports install nginx
```

**MYSQL**
```
apt-get -t wheezy-backports install mysql-client mysql-server
```

**CURL**
```
apt-get -t wheezy-backports install curl
```

**LAME**
```
apt-get -t wheezy-backports install lame
```

**NODE.JS**  
Тип системы смотрим так **uname -a**  
Берём версию linux binaries для своего типа системы тут https://nodejs.org/download/  
```
wget http://nodejs.org/dist/v0.12.5/node-v0.12.5-linux-x64.tar.gz
```
Распаковываем, копируем (например в mc под рутом) содержимое папки архива node-v* 
в папку /usr/local так чтобы перекрывались соответствующие папки  
```
node-v*/bin -> usr/local/bin
node-v*/include -> usr/local/include
node-v*/lib -> usr/local/lib
node-v*/share -> usr/local/share
```
Текстовики копировать не нужно

**GULP**
```
npm install -g gulp forever
```

**COMPOSER**
```
curl -sS https://getcomposer.org/installer | php
chmod +x composer.phar
mv composer.phar /usr/local/bin/composer
```

**PHP-FPM**
```
vim /etc/php5/fpm/pool.d/www.conf
```
Находим и заменяем строку
```
;listen.mode = 0660
listen.mode = 0666
```

## Настройки доступов, разворачиваем проект

Добавляем пользователя
```
adduser fitcons
```
Проходим мастер настройки пользователя.  
Добавляем нового пользователя в группу www-data для доступа к файлам проекта
```
usermod -a -G www-data fitcons
```
Авторизуемся под новым пользователем
```
su -l fitcons
```
Переходим в домашнюю папку нового пользователя
```
cd ~
```
Папка для версий проекта
```
mkdir www-data
cd www-data
```
Для каждой версии проекта (dev, stage, master и т.п.) выбираем нужную ветку  
Правим права на файлы готовым скриптом (см. скрипт fix-dev-access.sh в корне проекта), это придется делать под рутом, т.к. меняются группы доступа для файлов
```
git clone git@bitbucket.org:fitconsultant/web-server.git dev
cd www-data/dev
git checkout dev
./fix-dev-access.sh

git clone git@bitbucket.org:fitconsultant/web-server.git stage
cd www-data/stage
git checkout stage
./fix-dev-access.sh

git clone git@bitbucket.org:fitconsultant/web-server.git master
cd www-data/master
git checkout master
./fix-dev-access.sh
```
В каждой папке версии проекта выполняем
```
composer install; npm install; gulp
```
В каждой папке версии проекта копируем файлы настроек и генерируем ключ  
(см. в **.env** настройки бд, ключи, выбор окружения, режим отладки)
```
cp .env.example .env
./artisan key:generate
```
Настраиваем пользователя mysql для наших версий.  
Авторизуемся под рутом в mysql
```
mysql -u root -p
```
Создаем пользователя для отладки и бд для каждой версии проекта
```
CREATE USER fitcons_dev@localhost IDENTIFIED BY 'dev-fitcons-pass-232';
create database fitcons_master charset=utf8;
create database fitcons_dev charset=utf8;
create database fitcons_stage charset=utf8;
```
Выдаем права пользователю на базы соответствующих версий
```
GRANT ALL PRIVILEGES ON fitcons_master.* TO fitcons_dev@localhost;
GRANT ALL PRIVILEGES ON fitcons_dev.* TO fitcons_dev@localhost;
GRANT ALL PRIVILEGES ON fitcons_stage.* TO fitcons_dev@localhost;
flush privileges;
```
Cоздаем каталог для логов (зачем - смотри конфиг в проекте project-etc/fit-consultant.4dev.nginx.conf)
```
cd www-data
mkdir log
```
Под рутом меняем на него права
```
cd www-data
chmod 775 log
chown fitcons:www-data log
```
Для каждой версии проекта правим файлы настроек, правим там настройки БД
```
vim www-data/master/.env
vim www-data/dev/.env
vim www-data/stage/.env
```
Для каждой версии проекта инициализируем бд
```
./artisan migrate
./artisan db:seed
```
Конфиги nginx смотрим тут **project-etc/nginx-config**  
Переносим конфиги, правим пути, перечитываем конфиги (под рутом)
```
nginx -t
nginx -s reload
```
Для загрузки больших файлов правим конфиг PHP (fpm)  
**vim php.ini** 
```
max_execution_time = 600
upload_max_filesize = 200M
post_max_size = 200M
```
