# 1FitChat, документация, настройки, доп. информация

### REST API

* [Базовая часть функционала](rest-base.md)
* [Функционал для платежей через cloudpayments.ru](rest-payment.md)
* [Чаты](rest-chat.md)


### Дополнительная информация

* [Конфигурирование сервера под проект на Debian 7](server-config-help-debian7.md)
* [Настройки CORS для бакета на Amazon AWS](aws-bucket-cors-config.md)
* [Правила загрузки файлов на amazon s3](s3-file-uploads-rules.md)
* [Настройки cron](crontab-tasks.md)

