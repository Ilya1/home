# 1FitChat, Правила загрузки файлов на amazon s3

## Аватары
avatar/48x48/**{user_id}**.jpg  
avatar/180x200/**{user_id}**.jpg  

## Картинки
images/**{room_id}**/**{timestamp}**_file_name.jpg  

## Видео
video/**{room_id}**/**{timestamp}**_file_name.ext  

## Файлы
file/**{room_id}**/**{timestamp}**_file_name.ext  

## Аудио
audio/**{room_id}**/**{timestamp}**_file_name.ext

## Голосовые сообщения
audio-record/**{timestamp}**_**{salt}**.mp3

## Для сообщений содержащих файлы
Все происходит аналогично отправке обычного сообщения  
(см. метод **/rest/chat/send-message** в документации по REST методам для чата)  

В поле **type** необходимо указать тип сообщения (text,image,video,file,audio,audio_voice,system)  
В поле **text** в случае, если сообщение НЕ текстовое, нужно указать имя загруженного 
файла для того, чтобы можно было отобразить его в интерфейсе чата  
По системным сообщениям см. отдельный документ



